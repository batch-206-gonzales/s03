import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //2
        HashMap<String, Integer> games = new HashMap<>();

        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        //3
        games.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        //4-6
        ArrayList<String> topGames = new ArrayList<>();

        games.forEach((key,value) -> {
            if(value <= 30) {
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            };
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        //Stretch Goal
        Boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");

            Scanner addItemScanner = new Scanner(System.in);

            String input = addItemScanner.nextLine();

            if(input.equals("Yes")){
                System.out.println("Add the item name:");
                String itemName = addItemScanner.nextLine();

                System.out.println("Add the item stock:");
                Integer itemStock = addItemScanner.nextInt();

                games.put(itemName, itemStock);
                System.out.println(games);
                addItem = false;
            } else if(input.equals("No")) {
                addItem = false;
                System.out.println("Thank you!");
            } else {
                System.out.println("Invalid input. Try again.");
            }
        }
    }
}